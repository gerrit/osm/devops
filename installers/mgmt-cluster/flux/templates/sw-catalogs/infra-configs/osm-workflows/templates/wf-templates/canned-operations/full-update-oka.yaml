#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: full-update-oka-wtf
  namespace: osm-workflows
spec:
  arguments:
    parameters:
    # SW-Catalogs repo
    - name: git_sw_catalogs_url
    - name: sw_catalogs_destination_folder
    - name: git_sw_catalogs_cred_secret
    # Temporary volume with OKA contents
    - name: temp_volume_name
    # Specific parameters - OKA
    - name: oka_name
    ## Choose among `infra-controllers`, `infra-configs`, `cloud-resources`, `apps`:
    - name: oka_type
    - name: project_name
      value: "osm_admin"
    - name: tarball_file
      value: "true"
    # Debug/dry run?
    - name: debug
      value: "false"
    - name: dry_run
      value: "false"

  # Cleanup policy
  ttlStrategy:
    secondsAfterCompletion: 6000  # Time to live after workflow is completed, replaces ttlSecondsAfterFinished
    secondsAfterSuccess: 6000     # Time to live after workflow is successful
    secondsAfterFailure: 9000     # Time to live after workflow fails

  entrypoint: update-oka

  templates:
  # Main template
  - name: update-oka
    inputs:
      parameters:
      - name: git_sw_catalogs_url
      - name: sw_catalogs_destination_folder
      - name: git_sw_catalogs_cred_secret
      - name: temp_volume_name
      - name: oka_name
      - name: oka_type
      - name: project_name
      - name: tarball_file
      - name: debug
      - name: dry_run
    steps:
    # ------ Preparations for transaction
    - - name: generate-sw-catalogs-volume-repo
        templateRef:
          name: k8s-resources-wft
          template: generate-volume
        arguments:
          parameters:
            - name: pvc-size
              value: '100Mi'
    - - name: clone-sw-catalogs
        templateRef:
          name: git-wft
          template: git-clone
        arguments:
          parameters:
          - name: mount_path
            value: "/sw-catalogs"
          - name: repo_url
            value: "{{inputs.parameters.git_sw_catalogs_url}}"
          - name: destination_folder
            value: "{{inputs.parameters.sw_catalogs_destination_folder}}"
          - name: git_cred_secret
            value: "{{inputs.parameters.git_sw_catalogs_cred_secret}}"
          - name: git_volume_name
            value: '{{steps.generate-sw-catalogs-volume-repo.outputs.parameters.pvc-name}}'
    # ------ end of preparations for transaction

    # ------ Transformations
    - - name: update-oka
        templateRef:
          name: oka-management-wft
          template: update-oka
        arguments:
          parameters:
          # References to required external resources
          - name: sw_catalogs_volume_name
            value: '{{steps.generate-sw-catalogs-volume-repo.outputs.parameters.pvc-name}}'
          - name: temp_volume_name
            value: "{{inputs.parameters.temp_volume_name}}"
          # Specific parameters
          - name: oka_name
            value: "{{inputs.parameters.oka_name}}"
          - name: oka_type
            value: "{{inputs.parameters.oka_type}}"
          - name: project_name
            value: "{{inputs.parameters.project_name}}"
          - name: tarball_file
            value: "{{inputs.parameters.tarball_file}}"
          # Debug?
          - name: debug
            value: "{{inputs.parameters.debug}}"
    # ------ end of transformations

    # ------ Commit transaction
    - - name: push-to-sw-catalogs
        templateRef:
          name: git-wft
          template: git-commit-merge-push
        arguments:
          parameters:
          - name: mount_path
            value: "/sw-catalogs"
          - name: repo_folder
            value: "{{inputs.parameters.sw_catalogs_destination_folder}}"
          - name: git_cred_secret
            value: "{{inputs.parameters.git_sw_catalogs_cred_secret}}"
          - name: git_volume_name
            value: '{{steps.generate-sw-catalogs-volume-repo.outputs.parameters.pvc-name}}'
          - name: commit_message
            value: "Update OKA {{inputs.parameters.oka_name}} of {{inputs.parameters.oka_type}} type @ {{inputs.parameters.project_name}} project"
          - name: main_branch
            value: main
          - name: contrib_branch
            value: osm_contrib
          - name: dry_run
            value: "{{inputs.parameters.dry_run}}"
# ------ end of commit transaction

#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: full-delete-crossplane-providerconfig
  namespace: osm-workflows

spec:
  arguments:
    parameters:
    # Fleet repo
    - name: git_fleet_url
    - name: fleet_destination_folder
    - name: git_fleet_cred_secret
    # SW-Catalogs repo
    - name: git_sw_catalogs_url
    - name: sw_catalogs_destination_folder
    - name: git_sw_catalogs_cred_secret

    # Specific parameters
    - name: providerconfig_name
    ## As of today, one among `azure`, `aws` or `gcp`
    - name: provider_type
    - name: osm_project_name
      value: "osm_admin"
    # Debug/dry run?
    - name: debug
      value: "false"
    - name: dry_run
      value: "false"

  # Cleanup policy
  ttlStrategy:
    secondsAfterCompletion: 6000  # Time to live after workflow is completed, replaces ttlSecondsAfterFinished
    secondsAfterSuccess: 6000     # Time to live after workflow is successful
    secondsAfterFailure: 9000     # Time to live after workflow fails

  entrypoint: delete-crossplane-providerconfig

  templates:

  # Main template
  - name: delete-crossplane-providerconfig
    inputs:
      parameters:
      # Fleet repo
      - name: git_fleet_url
      - name: fleet_destination_folder
      - name: git_fleet_cred_secret
      # SW-Catalogs repo
      - name: git_sw_catalogs_url
      - name: sw_catalogs_destination_folder
      - name: git_sw_catalogs_cred_secret
      # Specific parameters
      - name: providerconfig_name
      - name: provider_type
      - name: osm_project_name
      # Debug/dry run?
      - name: debug
        value: "false"
      - name: dry_run
        value: "false"

    steps:

    # ------ Preparations for transaction
    - - name: generate-fleet-volume-repo
        templateRef:
          name: k8s-resources-wft
          template: generate-volume
        arguments:
          parameters:
            - name: pvc-size
              value: '100Mi'
      - name: generate-sw-catalogs-volume-repo
        templateRef:
          name: k8s-resources-wft
          template: generate-volume
        arguments:
          parameters:
            - name: pvc-size
              value: '100Mi'
    - - name: clone-fleet
        templateRef:
          name: git-wft
          template: git-clone
        arguments:
          parameters:
          - name: mount_path
            value: "/fleet"
          - name: repo_url
            value: "{{inputs.parameters.git_fleet_url}}"
          - name: destination_folder
            value: "{{inputs.parameters.fleet_destination_folder}}"
          - name: git_cred_secret
            value: "{{inputs.parameters.git_fleet_cred_secret}}"
          - name: git_volume_name
            value: '{{steps.generate-fleet-volume-repo.outputs.parameters.pvc-name}}'
      - name: clone-sw-catalogs
        templateRef:
          name: git-wft
          template: git-clone
        arguments:
          parameters:
          - name: mount_path
            value: "/sw-catalogs"
          - name: repo_url
            value: "{{inputs.parameters.git_sw_catalogs_url}}"
          - name: destination_folder
            value: "{{inputs.parameters.sw_catalogs_destination_folder}}"
          - name: git_cred_secret
            value: "{{inputs.parameters.git_sw_catalogs_cred_secret}}"
          - name: git_volume_name
            value: '{{steps.generate-sw-catalogs-volume-repo.outputs.parameters.pvc-name}}'
    # ------ end of preparations for transaction

    # ------ Transformations
    - - name: delete-crossplane-providerconfig
        templateRef:
          name: cloud-accounts-wft
          template: delete-crossplane-providerconfig
        arguments:
          parameters:
          # References to required external resources
          - name: fleet_volume_name
            value: '{{steps.generate-fleet-volume-repo.outputs.parameters.pvc-name}}'
          - name: sw_catalogs_volume_name
            value: '{{steps.generate-sw-catalogs-volume-repo.outputs.parameters.pvc-name}}'
          # Specific parameters
          - name: providerconfig_name
            value: "{{inputs.parameters.providerconfig_name}}"
          - name: provider_type
            value: "{{inputs.parameters.provider_type}}"
          - name: osm_project_name
            value: "{{inputs.parameters.osm_project_name}}"
          # Debug?
          - name: debug
            value: "{{inputs.parameters.debug}}"
    # ------ end of transformations

    # ------ Commit transaction
    - - name: push-to-fleet
        templateRef:
          name: git-wft
          template: git-commit-merge-push
        arguments:
          parameters:
          - name: mount_path
            value: "/fleet"
          - name: repo_folder
            value: "{{inputs.parameters.fleet_destination_folder}}"
          - name: git_cred_secret
            value: "{{inputs.parameters.git_fleet_cred_secret}}"
          - name: git_volume_name
            value: '{{steps.generate-fleet-volume-repo.outputs.parameters.pvc-name}}'
          - name: commit_message
            value: "Delete ProviderConfig {{inputs.parameters.providerconfig_name}} for {{inputs.parameters.provider_type}}"
          - name: main_branch
            value: main
          - name: contrib_branch
            value: osm_contrib
          - name: dry_run
            value: "{{inputs.parameters.dry_run}}"
# ------ end of commit transaction

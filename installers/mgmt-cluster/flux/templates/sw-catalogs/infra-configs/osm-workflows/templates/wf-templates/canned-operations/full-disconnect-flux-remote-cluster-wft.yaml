#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: full-disconnect-flux-remote-cluster-wft
  namespace: osm-workflows

spec:
  arguments:
    parameters:

    # Fleet repo
    - name: git_fleet_url
    - name: fleet_destination_folder
    - name: git_fleet_cred_secret

    # Specific parameters
    - name: cluster_kustomization_name
    - name: mgmt_project_name
      value: "osm_admin"

    # Debug/dry run?
    - name: debug
      value: "false"
    - name: dry_run
      value: "false"


  # # Cleanup policy
  # ttlStrategy:
  #   secondsAfterCompletion: 100 # Time to live after workflow is completed, replaces ttlSecondsAfterFinished
  #   secondsAfterSuccess: 50     # Time to live after workflow is successful
  #   secondsAfterFailure: 50     # Time to live after workflow fails

  entrypoint: disconnect-remote-cluster

  templates:

  # Main template
  - name: disconnect-remote-cluster
    inputs:
      parameters:
      # Git repos
      - name: git_fleet_url
      - name: fleet_destination_folder
      - name: git_fleet_cred_secret

      # Specific parameters
      - name: cluster_kustomization_name
      - name: mgmt_project_name

      # Debugging
      - name: debug
      - name: dry_run
    steps:

    # ------ Preparations for transaction
    - - name: generate-fleet-volume-repo
        templateRef:
          name: k8s-resources-wft
          template: generate-volume
        arguments:
          parameters:
            - name: pvc-size
              value: '100Mi'
    - - name: clone-fleet
        templateRef:
          name: git-wft
          template: git-clone
        arguments:
          parameters:
          - name: mount_path
            value: "/fleet"
          - name: repo_url
            value: "{{inputs.parameters.git_fleet_url}}"
          - name: destination_folder
            value: "{{inputs.parameters.fleet_destination_folder}}"
          - name: git_cred_secret
            value: "{{inputs.parameters.git_fleet_cred_secret}}"
          - name: git_volume_name
            value: '{{steps.generate-fleet-volume-repo.outputs.parameters.pvc-name}}'
    # ------ end of preparations for transaction

    # ------ Transformations
    # Disconnect the remote cluster
    - - name: disconnect-flux-remote-cluster
        templateRef:
          name: cluster-management-wft
          template: disconnect-flux-remote-cluster
        arguments:
          parameters:
          # Volumes with cloned repos
          - name: fleet_volume_name
            value: '{{steps.generate-fleet-volume-repo.outputs.parameters.pvc-name}}'

          # Specific parameters
          - name: cluster_kustomization_name
            value: "{{inputs.parameters.cluster_kustomization_name}}"
          - name: mgmt_project_name
            value: "{{inputs.parameters.mgmt_project_name}}"
    # ------ end of transformations

    # ------ Commit transaction
    - - name: push-to-fleet
        templateRef:
          name: git-wft
          template: git-commit-merge-push
        arguments:
          parameters:
          - name: mount_path
            value: "/fleet"
          - name: repo_folder
            value: "{{inputs.parameters.fleet_destination_folder}}"
          - name: git_cred_secret
            value: "{{inputs.parameters.git_fleet_cred_secret}}"
          - name: git_volume_name
            value: '{{steps.generate-fleet-volume-repo.outputs.parameters.pvc-name}}'
          - name: commit_message
            value: "Disconnect imported cluster {{inputs.parameters.cluster_kustomization_name}}"
          - name: main_branch
            value: main
          - name: contrib_branch
            value: osm_contrib
          - name: dry_run
            value: "{{inputs.parameters.dry_run}}"
# ------ end of commit transaction
